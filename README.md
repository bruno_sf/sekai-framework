# Sekai Framework #
A framework that allows you to create map configurations based on models and game objects.

A Model is literally a "template configuration" with default values that can be reused on other GameObject. You can also imagine them as a "prefab". Each GameObject implementation must have an unique model implementation, for example: SkyWarsChest must have a SkyWarsChestModel implementation.

A GameObject can be anything, a SkyWarsChest, an ItemDispenser, an AreaTrigger, etc

A GameObject can, or not, be based from a model, and if it is based on a model, you can override the model's values without modifying the model itself, these modified values are part of the GameObject (W.I.P.), this allow you to make small changes to a specific Game Object if you wish to. And a modified GameObject can be exported as a new model based on the current object's parameters.

I'll post a Maven repo soon