package me.ddevil.sekai.example;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import me.ddevil.sekai.api.game.GameObject;
import me.ddevil.sekai.api.misc.MapLocation;
import me.ddevil.sekai.api.misc.NamedLocation;
import me.ddevil.sekai.example.resource.ExampleMapResources;
import me.ddevil.sekai.example.resource.ExampleMapProvider;
import me.ddevil.sekai.modelpack.one.ModelPackOneLocalResourceHandler;
import me.ddevil.sekai.modelpack.one.healthregenerator.HealthRegenerator;
import me.ddevil.sekai.modelpack.one.pickupstation.PickupStation;
import me.ddevil.sekai.internal.map.BaseFFAGameMap;
import me.ddevil.shiroi.utils.item.MinecraftIcon;
import me.ddevil.shiroi.utils.serialization.SerializationUtils;
import org.json.simple.parser.ParseException;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by bruno on 11/10/2016.
 */
public class ExampleMap extends BaseFFAGameMap<ExampleMapResources> {
    public static final String INTEREST_POINTS_IDENTIFIER = "interestPoints";
    public static final String PICKUP_STATIONS_IDENTIFIER = "pickupStations";
    public static final String HEALTH_REGENERATORS_IDENTIFIER = "healthRegenerators";
    @Nonnull
    private final List<NamedLocation> interestPoints;
    @Nonnull
    private final List<PickupStation> pickupStations;
    @Nonnull
    private final List<HealthRegenerator> healthRegenerators;


    public ExampleMap(String name, String alias, @Nonnull List<String> description, @Nonnull List<MapLocation> spawnPoints, int maxPlayers, @Nonnull MinecraftIcon icon, ExampleMapResources resources) {
        super(name, alias, resources, icon, description, spawnPoints, maxPlayers);
        this.interestPoints = new ArrayList<>();
        this.pickupStations = new ArrayList<>();
        this.healthRegenerators = new ArrayList<>();
    }

    public ExampleMap(Map<String, Object> map, ExampleMapProvider provider, ModelPackOneLocalResourceHandler resources) {
        super(map, provider);
        List<Map<String, Object>> serializedInterestPoints = (List<Map<String, Object>>) map.get(INTEREST_POINTS_IDENTIFIER);
        this.interestPoints = SerializationUtils.deserializeList(serializedInterestPoints, NamedLocation::new);
        List<Map<String, Object>> serializedPickupStations = (List<Map<String, Object>>) map.get(PICKUP_STATIONS_IDENTIFIER);
        this.pickupStations = SerializationUtils.deserializeList(serializedPickupStations, map1 -> {
            try {
                return new PickupStation(map1, resources);
            } catch (Exception e) {
                throw new IllegalStateException("An error occurred while loading a PickupStation!", e);
            }
        });
        List<Map<String, Object>> serializedRegenerators = (List<Map<String, Object>>) map.get(HEALTH_REGENERATORS_IDENTIFIER);
        this.healthRegenerators = SerializationUtils.deserializeList(serializedRegenerators, map12 -> {
            try {
                return new HealthRegenerator(map12, resources);
            } catch (ParseException | IOException | ClassNotFoundException e) {
                throw new IllegalStateException("An error occurred while loading a HealthRegenerator!", e);
            }
        });
    }

    @Override
    protected void addObject0(GameObject<?> object) {
        if (object instanceof PickupStation) {
            pickupStations.add((PickupStation) object);
        }
        if (object instanceof HealthRegenerator) {
            healthRegenerators.add((HealthRegenerator) object);
        }
    }

    @Override
    @Nonnull
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .putAll(super.serialize())
                .put(PICKUP_STATIONS_IDENTIFIER, SerializationUtils.serializeList(pickupStations))
                .put(INTEREST_POINTS_IDENTIFIER, SerializationUtils.serializeList(interestPoints))
                .put(HEALTH_REGENERATORS_IDENTIFIER, SerializationUtils.serializeList(healthRegenerators))
                .build();
    }

    public void addInterestPoint(NamedLocation namedLocation) {
        if (!interestPoints.contains(namedLocation)) {
            interestPoints.add(namedLocation);
        }
    }

    @Override
    public Set<GameObject<?>> getGameObjects() {
        return new ImmutableSet.Builder<GameObject<?>>()
                .addAll(pickupStations)
                .build();
    }

    @Override
    public void removeObject(@Nonnull GameObject<?> object) {

    }
}
