package me.ddevil.sekai.example.resource;

import me.ddevil.sekai.example.ExampleMap;
import me.ddevil.sekai.internal.resource.LocalMapProvider;
import me.ddevil.sekai.modelpack.one.ModelPackOneLocalResourceHandler;
import org.json.simple.JSONObject;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.Map;

/**
 * Created by bruno on 11/10/2016.
 */
public class ExampleMapProvider extends LocalMapProvider<ExampleMap, ExampleMapResources> {

    @Nonnull
    private ModelPackOneLocalResourceHandler modelPackProvider;

    public ExampleMapProvider(@Nonnull File resourcesFolder, @Nonnull ModelPackOneLocalResourceHandler modelPackProvider) {
        super(resourcesFolder);
        this.modelPackProvider = modelPackProvider;
    }

    @Nonnull
    public ModelPackOneLocalResourceHandler getModelPackProvider() {
        return modelPackProvider;
    }

    public void setModelPackProvider(@Nonnull ModelPackOneLocalResourceHandler modelPackProvider) {
        this.modelPackProvider = modelPackProvider;
    }

    @Override
    protected ExampleMap loadMap0(JSONObject json) {
        return new ExampleMap(json, this, modelPackProvider);
    }


    @Override
    public ExampleMapResources loadResources(Map<String, Object> o) {
        return new ExampleMapResources(o, modelPackProvider);
    }
}
