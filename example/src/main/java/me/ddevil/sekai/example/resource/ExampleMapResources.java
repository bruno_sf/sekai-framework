package me.ddevil.sekai.example.resource;


import com.google.common.collect.ImmutableMap;
import me.ddevil.sekai.api.game.GameObject;
import me.ddevil.sekai.api.model.Model;
import me.ddevil.sekai.modelpack.one.ModelPackOneLocalResourceHandler;
import me.ddevil.sekai.modelpack.one.healthregenerator.HealthRegeneratorModel;
import me.ddevil.sekai.modelpack.one.pickupstation.PickupStationModel;
import me.ddevil.sekai.exception.ModelDependencyNotFoundException;
import me.ddevil.sekai.internal.map.BaseGameMapResources;
import me.ddevil.shiroi.utils.serialization.SerializationUtils;
import org.json.simple.parser.ParseException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;

/**
 * Created by bruno on 11/10/2016.
 */
public class ExampleMapResources extends BaseGameMapResources {
    public static final String PICKUP_STATION_MODEL_IDENTIFIER = "pickupStationModels";
    private static final String HEALTH_REGENERATOR_MODEL_IDENTIFIER = "healthRegeneratorModels";
    private final List<PickupStationModel> pickupStationModels;
    private final List<HealthRegeneratorModel> healthRegeneratorModels;

    public ExampleMapResources() {
        pickupStationModels = new ArrayList<>();
        healthRegeneratorModels = new ArrayList<>();
    }

    public ExampleMapResources(Map<String, Object> map, ModelPackOneLocalResourceHandler provider) {
        super(map, provider);
        //Set this as the resource path for the handler
        List<String> requiredStationModel = (List<String>) map.get(PICKUP_STATION_MODEL_IDENTIFIER);
        this.pickupStationModels = SerializationUtils.deserializeListFromReference(requiredStationModel, s -> {
            try {
                return provider.provide(s, PickupStationModel.class, Optional.of(this));
            } catch (Exception e) {
                throw new ModelDependencyNotFoundException(e, s, PickupStationModel.class);
            }
        });
        List<String> requiredRegeneratorModels = (List<String>) map.get(HEALTH_REGENERATOR_MODEL_IDENTIFIER);
        this.healthRegeneratorModels = SerializationUtils.deserializeListFromReference(requiredRegeneratorModels, s -> {
            try {
                return provider.provide(s, HealthRegeneratorModel.class, Optional.of(ExampleMapResources.this));
            } catch (IOException | ParseException e) {
                throw new ModelDependencyNotFoundException(e, s, HealthRegeneratorModel.class);
            }
        });
    }

    @Nonnull
    @Override
    public <M extends Model> List<M> getLoadedModels(Class<M> modelClass) {
        return Collections.emptyList();
    }

    @Nullable
    @Override
    public <M extends Model> M getModel(String modelName, Class<M> modelClass) {
        return null;
    }

    @Nullable
    @Override
    public Model getModel(String modelName) {
        return null;
    }

    @Nonnull
    @Override
    public Map<String, Model> getAllModelsMap0() {
        ImmutableMap.Builder<String, Model> builder = new ImmutableMap.Builder<>();
        pickupStationModels.forEach(pickupStationModel -> builder.put(pickupStationModel.getName(), pickupStationModel));
        healthRegeneratorModels.forEach(healthRegeneratorModel -> builder.put(healthRegeneratorModel.getName(), healthRegeneratorModel));
        return builder.build();
    }

    @Nonnull
    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .putAll(super.serialize())
                .put(PICKUP_STATION_MODEL_IDENTIFIER, SerializationUtils.serializeListToReference(pickupStationModels))
                .put(HEALTH_REGENERATOR_MODEL_IDENTIFIER, SerializationUtils.serializeListToReference(healthRegeneratorModels))
                .build();
    }

    @Override
    public void clean(List<GameObject<?>> objects) {

    }

    @Override
    protected void addResource0(Model resource) {
        if (resource instanceof PickupStationModel) {
            pickupStationModels.add((PickupStationModel) resource);
        } else if (resource instanceof HealthRegeneratorModel) {
            healthRegeneratorModels.add((HealthRegeneratorModel) resource);
        } else {
            throw new UnsupportedOperationException(getClass().getName() + " doesn't support model " + resource.getFullName() + "!");
        }
    }
}
