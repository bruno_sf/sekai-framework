package me.ddevil.sekai.example.test;

import com.google.common.collect.ImmutableList;
import me.ddevil.sekai.api.misc.MapLocation;
import me.ddevil.sekai.api.misc.MapSetting;
import me.ddevil.sekai.example.ExampleMap;
import me.ddevil.sekai.example.resource.ExampleMapProvider;
import me.ddevil.sekai.api.misc.NamedLocation;
import me.ddevil.sekai.example.resource.ExampleMapResources;
import me.ddevil.sekai.modelpack.one.ModelPackOneLocalResourceHandler;
import me.ddevil.sekai.modelpack.one.healthregenerator.HealthRegenerator;
import me.ddevil.sekai.modelpack.one.healthregenerator.HealthRegeneratorModel;
import me.ddevil.sekai.modelpack.one.pickupstation.PickupStation;
import me.ddevil.sekai.modelpack.one.pickupstation.PickupStationModel;
import me.ddevil.shiroi.utils.item.Item;
import me.ddevil.shiroi.utils.item.Material;
import me.ddevil.shiroi.utils.item.MinecraftIcon;
import org.json.simple.JSONObject;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * Created by bruno on 11/10/2016.
 */
public class ExampleGameMapTest {
    private static final String EXAMPLE_MAP_NAME = "exampleMap";
    private static final String EXAMPLE_MAP_ALIAS = "Hanamura";
    private static final List<String> EXAMPLE_MAP_DESCRIPTION = Arrays.asList(
            "Hanamura is a nice map actually :D",
            "Unless you are defending and you",
            "don't have a Symmetra on your team...",
            "If you don't, maaaaan...",
            "You'll have to walk a LOT!");
    private static final List<HealthRegeneratorModel> HEALTH_REGENERATOR_MODELS = new ImmutableList.Builder<HealthRegeneratorModel>()
            .add(new HealthRegeneratorModel("highRateRegenerator", "High Rate Regenerator", 5d, 0.1d, 2L))
            .add(new HealthRegeneratorModel("lowRateRegenerator", "Low Rate Regenerator", 10d, 5d, 40L))
            .build();
    private static final List<HealthRegenerator> HEALTH_REGENERATORS = new ImmutableList.Builder<HealthRegenerator>()
            .add(new HealthRegenerator("mainRegenerator", "Regenerator principal", new MapLocation(10d, 32d, 10d), HEALTH_REGENERATOR_MODELS.get(0)))
            .build();
    private static final File SAVE_FOLDER = new File("C:\\Work\\projects\\sekai-framework\\example\\src\\main\\resources");
    //private static final File SAVE_FOLDER = new File(System.getProperty("user.home") + "/SekaiModels");

    private static final List<NamedLocation> NAMED_LOCATIONS = new ImmutableList.Builder<NamedLocation>()
            .add(new NamedLocation("centralPlaza", "Central Plaza", 120, 50, 100))
            .build();
    private static final List<MapLocation> SPAWN_POINTS = new ImmutableList.Builder<MapLocation>()
            .add(new MapLocation(125, 50, 100))
            .build();
    private static final List<MapSetting> MAP_SETTINGS = new ImmutableList.Builder<MapSetting>()
            .add(new MapSetting("easy", "大丈夫んだ", 0, new MinecraftIcon.Builder(Material.IRON_AXE).build()))
            .add(new MapSetting("hard", "お前は死んで", 3, new MinecraftIcon.Builder(Material.DIAMOND_AXE).build()))
            .build();
    private static final List<PickupStationModel> PICKUP_STATION_MODELS = new ImmutableList.Builder<PickupStationModel>()
            .add(new PickupStationModel("stationOne", "スタション一", new Item(Material.IRON_AXE)))
            .add(new PickupStationModel("stationTwo", "スタション二", new Item(Material.IRON_SWORD)))
            .add(new PickupStationModel("stationThree", "スタション三", new Item(Material.DIAMOND_SWORD)))
            .build();
    private static final List<PickupStation> PICKUP_STATIONS = new ImmutableList.Builder<PickupStation>()
            .add(new PickupStation("mainStation", "何俺がやってんだの?", new MapLocation(23, 12, 42), PICKUP_STATION_MODELS.get(0)))
            .build();

    @Test
    public void serializationTest() {
        ExampleMapResources resources = new ExampleMapResources();
        ExampleMap map = new ExampleMap(
                EXAMPLE_MAP_NAME,
                EXAMPLE_MAP_ALIAS,
                EXAMPLE_MAP_DESCRIPTION,
                SPAWN_POINTS,
                12,
                new MinecraftIcon.Builder(Material.DIAMOND_SWORD).build(),
                resources);
        MAP_SETTINGS.forEach(resources::addResource);
        PICKUP_STATION_MODELS.forEach(resources::addResource);
        HEALTH_REGENERATOR_MODELS.forEach(resources::addResource);
        PICKUP_STATIONS.forEach(map::addObject);
        HEALTH_REGENERATORS.forEach(map::addObject);
        NAMED_LOCATIONS.forEach(namedLocation -> {
            System.out.println("Adding location " + namedLocation.getName() + "...");
            map.addInterestPoint(namedLocation);
        });

        System.out.println("GameMap " + map.getFullName() + " to JSON:");
        System.out.println(new JSONObject(map.serialize()).toJSONString());
        System.out.println("Saving map...");
        ModelPackOneLocalResourceHandler modelProvider = new ModelPackOneLocalResourceHandler(SAVE_FOLDER);
        ExampleMapProvider provider = new ExampleMapProvider(SAVE_FOLDER, modelProvider);
        try {
            provider.save(map, modelProvider);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deserializationTest() {
        ExampleMapProvider exampleMapResourcesHandler = new ExampleMapProvider(SAVE_FOLDER, new ModelPackOneLocalResourceHandler(SAVE_FOLDER));
        try {
            ExampleMap map = exampleMapResourcesHandler.loadMap(EXAMPLE_MAP_NAME);
            System.out.println(new JSONObject(map.serialize()).toJSONString());
            map.getResources().forEach(gameObject -> System.out.println(new JSONObject(gameObject.serialize()).toJSONString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
