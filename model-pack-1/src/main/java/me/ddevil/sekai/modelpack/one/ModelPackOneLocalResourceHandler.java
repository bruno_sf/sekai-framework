package me.ddevil.sekai.modelpack.one;

import com.google.common.collect.ImmutableList;
import me.ddevil.sekai.api.model.Model;
import me.ddevil.sekai.api.resource.GameMapResources;
import me.ddevil.sekai.internal.resource.LocalModelProvider;
import me.ddevil.sekai.modelpack.one.pickupstation.PickupStationModel;
import org.json.simple.JSONObject;

import java.io.File;
import java.util.List;
import java.util.Optional;

/**
 * Created by bruno on 17/10/2016.
 */
public class ModelPackOneLocalResourceHandler extends LocalModelProvider<GameMapResources> {
    public ModelPackOneLocalResourceHandler(File resourcesFolder) {
        super(resourcesFolder);
    }


    @Override
    public List<Class<? extends Model>> getHandledTypes() {
        return new ImmutableList.Builder<Class<? extends Model>>()
                .add(PickupStationModel.class)
                .build();
    }

    @Override
    public <M extends Model> M provide(JSONObject json, Class<M> modelClass, Optional<GameMapResources> resources) {
        if (modelClass.equals(PickupStationModel.class)) {
            return (M) new PickupStationModel(json);
        }
        throw new UnsupportedOperationException("Cannot provide model " + modelClass.toGenericString());
    }

}
