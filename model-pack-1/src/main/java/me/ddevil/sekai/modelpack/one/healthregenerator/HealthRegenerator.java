package me.ddevil.sekai.modelpack.one.healthregenerator;

import me.ddevil.sekai.SekaiConstants;
import me.ddevil.sekai.api.misc.MapLocation;
import me.ddevil.sekai.api.resource.ModelProvider;
import me.ddevil.sekai.internal.game.BaseGameObject;
import org.json.simple.parser.ParseException;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Map;

/**
 * Created by bruno on 17/10/2016.
 */
public class HealthRegenerator extends BaseGameObject<HealthRegeneratorModel> {


    public HealthRegenerator(@Nonnull Map<String, Object> map, ModelProvider<?> provider) throws ParseException, IOException, ClassNotFoundException {
        super(map, provider);
    }

    public HealthRegenerator(String name, String alias, MapLocation position, HealthRegeneratorModel model) {
        super(name, alias, position, model);
    }

    @Nonnull
    @Override
    public HealthRegeneratorModel exportToModel() {
        return new HealthRegeneratorModel(
                name + SekaiConstants.MODEL_EXPORT_FROM_OBJECT_SUFFIX,
                alias + SekaiConstants.MODEL_EXPORT_FROM_OBJECT_SUFFIX,
                model.getRange(),
                model.getHealAmount(),
                model.getUpdateRate()
        );
    }
}
