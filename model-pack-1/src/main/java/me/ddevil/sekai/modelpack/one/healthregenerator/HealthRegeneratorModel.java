package me.ddevil.sekai.modelpack.one.healthregenerator;

import com.google.common.collect.ImmutableMap;
import me.ddevil.sekai.SekaiConstants;
import me.ddevil.sekai.internal.model.IndependentModel;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * Created by bruno on 17/10/2016.
 */
public class HealthRegeneratorModel extends IndependentModel {
    private static final String HEAL_AMOUNT_IDENTIFIER = "healAmount";

    private double range;

    private double healAmount;

    private long updateRate;

    public HealthRegeneratorModel(String name, String alias, double range, double healAmount, long updateRate) {
        super(name, alias);
        this.range = range;
        this.healAmount = healAmount;
        this.updateRate = updateRate;
    }

    public HealthRegeneratorModel(@Nonnull Map<String, Object> map) {
        super(map);
        this.updateRate = ((Number) map.get(SekaiConstants.UPDATE_RATE_IDENTIFIER)).longValue();
        this.range = ((Number) map.get(SekaiConstants.RANGE_IDENTIFIER)).doubleValue();
        this.healAmount = ((Number) map.get(HEAL_AMOUNT_IDENTIFIER)).doubleValue();
    }

    @Nonnull
    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .putAll(super.serialize())
                .put(SekaiConstants.RANGE_IDENTIFIER, range)
                .put(SekaiConstants.UPDATE_RATE_IDENTIFIER, updateRate)
                .put(HEAL_AMOUNT_IDENTIFIER, healAmount)
                .build();
    }

    public double getRange() {
        return range;
    }

    public void setRange(double range) {
        this.range = range;
    }

    public double getHealAmount() {
        return healAmount;
    }

    public void setHealAmount(double healAmount) {
        this.healAmount = healAmount;
    }

    public long getUpdateRate() {
        return updateRate;
    }

    public void setUpdateRate(long updateRate) {
        this.updateRate = updateRate;
    }
}
