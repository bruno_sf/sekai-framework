package me.ddevil.sekai.modelpack.one.pickupstation;

import me.ddevil.sekai.SekaiConstants;
import me.ddevil.sekai.api.misc.MapLocation;
import me.ddevil.sekai.api.resource.ModelProvider;
import me.ddevil.sekai.internal.game.BaseGameObject;
import me.ddevil.shiroi.utils.item.Item;

import javax.annotation.Nonnull;
import java.util.Map;

public class PickupStation extends BaseGameObject<PickupStationModel> {
    private static final String OBJECT_ITEM_PICKUP_IDENTIFIER = "objectItem";
    private Item objectItem;

    public PickupStation(@Nonnull String name, @Nonnull String alias, @Nonnull MapLocation position, Item objectItem) {
        super(name, alias, position);
        this.objectItem = objectItem;
    }

    public PickupStation(@Nonnull Map<String, Object> map, ModelProvider<?> provider) throws Exception {
        super(map, provider);
        if (map.containsKey(OBJECT_ITEM_PICKUP_IDENTIFIER)) {
            this.objectItem = Item.deserialize((Map<String, Object>) map.get(OBJECT_ITEM_PICKUP_IDENTIFIER));
        }
    }

    public PickupStation(String name, String alias, MapLocation position, PickupStationModel model) {
        super(name, alias, position, model);
    }

    @Nonnull
    @Override
    public PickupStationModel exportToModel() {
        Item item;
        if (model != null) {
            item = model.getItem();
        } else if (this.objectItem != null) {
            item = this.objectItem;
        } else {
            throw new IllegalArgumentException("PickupStation doesn't have a model or an item to base from!");
        }
        return new PickupStationModel(name + SekaiConstants.MODEL_EXPORT_FROM_OBJECT_SUFFIX, alias + SekaiConstants.MODEL_EXPORT_FROM_OBJECT_SUFFIX, item);
    }
}
