package me.ddevil.sekai.modelpack.one.pickupstation;

import com.google.common.collect.ImmutableMap;
import me.ddevil.sekai.api.model.Model;
import me.ddevil.sekai.api.resource.GameMapResources;
import me.ddevil.sekai.internal.model.BaseModel;
import me.ddevil.sekai.internal.model.IndependentModel;
import me.ddevil.shiroi.utils.item.Item;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * Created by bruno on 11/10/2016.
 */
public class PickupStationModel extends IndependentModel {
    public static final String STATION_ITEM_IDENTIFIER = "item";
    private final Item item;

    public PickupStationModel(String name, String alias, Item item) {
        super(name, alias);
        this.item = item;
    }

    public PickupStationModel(Map<String, Object> map) {
        super(map);
        this.item = Item.deserialize((Map<String, Object>) map.get(STATION_ITEM_IDENTIFIER));
    }

    public Item getItem() {
        return item;
    }


    @Nonnull
    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .putAll(super.serialize())
                .put(STATION_ITEM_IDENTIFIER, item.serialize())
                .build();
    }

}
