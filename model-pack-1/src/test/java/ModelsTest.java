import me.ddevil.sekai.modelpack.one.ModelPackOneLocalResourceHandler;
import me.ddevil.sekai.modelpack.one.healthregenerator.HealthRegeneratorModel;
import me.ddevil.sekai.modelpack.one.pickupstation.PickupStationModel;
import org.json.simple.parser.ParseException;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * Created by bruno on 17/10/2016.
 */
public class ModelsTest {

    private static final File RESOURCE_FOLDER = new File("C:/Work/projects/sekai-framework/model-pack-1/src/test/resources/");

    @Test
    public void pickupStationTest() {
        HealthRegeneratorModel healthRegeneratorModel = new HealthRegeneratorModel("healthGenerator", "Banana", 10d, 0.25d, 5L);
        ModelPackOneLocalResourceHandler handler = new ModelPackOneLocalResourceHandler(RESOURCE_FOLDER);
        try {
            handler.save(healthRegeneratorModel);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
