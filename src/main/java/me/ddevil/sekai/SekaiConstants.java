package me.ddevil.sekai;

import java.util.logging.Logger;

public final class SekaiConstants {
    public static final String MODEL_EXTENSION = ".json";
    public static final String LOGGER_NAME = "SekaiFramework";
    public static final Logger LOGGER = Logger.getLogger(LOGGER_NAME);
    public static final String SPAWN_POINTS_IDENTIFIER = "spawnPoints";
    public static final String MAP_MAX_PLAYERS_IDENTIFIER = "maxPlayers";
    public static final String MAP_RESOURCES_IDENTIFIER = "resources";
    public static final String MODEL_EXPORT_FROM_OBJECT_SUFFIX = "_exported";
    public static final String RANGE_IDENTIFIER = "range";
    public static final String UPDATE_RATE_IDENTIFIER = "updateRate";

}
