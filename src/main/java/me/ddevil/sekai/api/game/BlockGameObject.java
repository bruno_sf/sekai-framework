package me.ddevil.sekai.api.game;

import me.ddevil.sekai.api.model.Model;
import me.ddevil.shiroi.misc.vector.BlockVector;

import java.util.List;

/**
 * Created by BRUNO II on 24/08/2016.
 */
public interface BlockGameObject<M extends Model> extends GameObject<M> {

    List<BlockVector> getBlocks();

}
