package me.ddevil.sekai.api.game;

import me.ddevil.sekai.api.model.Model;
import me.ddevil.sekai.api.misc.MapLocation;
import me.ddevil.shiroi.misc.Nameable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * // TODO: 15/10/2016 finish documentation
 *
 * @param <M> The model type that should be used with this GameObject.
 */
public interface GameObject<M extends Model> extends Nameable {
    @Nonnull
    MapLocation getPosition();

    void setPosition(@Nonnull MapLocation position);

    @Nullable
    M getModel();

    @Nonnull
    M exportToModel();

    void setModel(@Nullable M model);
}
