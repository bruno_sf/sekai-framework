package me.ddevil.sekai.api.map;


import me.ddevil.sekai.api.resource.GameMapResources;

/**
 * Created by BRUNO II on 22/08/2016.
 */
public interface CoopGameMap<R extends GameMapResources> extends GameMap<R> {
}
