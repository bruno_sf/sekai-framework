package me.ddevil.sekai.api.map;


import me.ddevil.sekai.api.misc.MapLocation;
import me.ddevil.sekai.api.resource.GameMapResources;

import java.util.List;

/**
 * Created by BRUNO II on 22/08/2016.
 */
public interface FFAGameMap<R extends GameMapResources> extends GameMap<R> {
    List<MapLocation> getSpawnPoints();
}
