package me.ddevil.sekai.api.map;

import me.ddevil.sekai.api.resource.Resource;
import me.ddevil.sekai.api.game.GameObject;
import me.ddevil.sekai.api.resource.GameMapResources;
import me.ddevil.shiroi.misc.Nameable;
import me.ddevil.shiroi.utils.item.MinecraftIcon;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Set;

/**
 * Represents a map configuration that will be used inside a minigame, obviously this isn't
 * the world itself, it contains info about how the map and it's objects should work.
 */
public interface GameMap<R extends GameMapResources> extends Resource, Nameable {
    /**
     * Gets the {@link GameMapResources} associated with this map.
     *
     * @return The {@link GameMapResources} associated with this map.
     */
    R getResources();

    /**
     * Changes the resource path used in by this map.
     *
     * @param resources The resource to be ser
     * @throws Exception If the given <code>resources</code> doesn't contain
     *                   every model required by the map's {@link GameObject}s to work.
     * @see GameMapResources
     */
    void setResources(R resources) throws IllegalStateException;

    /**
     * @return A set containing every single object in the map.
     */
    Set<GameObject<?>> getGameObjects();

    /**
     * @return The max numbers of players that can play on this map simultaneously
     */
    int getMaxPlayers();

    /**
     * Adds the specified object to the map.
     * <br>
     * The map implementation should handle how the object is stored/serialized and recovered/deserialized.
     *
     * @param object The object to be added
     */
    void addObject(@Nonnull GameObject<?> object);

    /**
     * Removes the given object from the map, duuuh, what did you expect?
     *
     * @param object The object to be removed
     */
    void removeObject(@Nonnull GameObject<?> object);

    /**
     * @return The icon of the map.
     */
    @Nonnull
    MinecraftIcon getIcon();

    /**
     * @param icon The icon to be set.
     */
    void setIcon(@Nonnull MinecraftIcon icon);

    void setDescription(@Nonnull List<String> description);

    @Nonnull
    List<String> getDescription();

}
