package me.ddevil.sekai.api.map;

import com.google.common.collect.Multimap;
import me.ddevil.sekai.api.resource.GameMapResources;
import me.ddevil.sekai.api.misc.MapLocation;
import me.ddevil.sekai.api.misc.Team;

import java.util.List;

/**
 * Created by BRUNO II on 21/08/2016.
 */
public interface TeamGameMap<R extends GameMapResources> extends GameMap<R> {

    void addSpawnPoint(Team team, MapLocation point);

    boolean hasTeam(Team name);

    List<MapLocation> getSpawnPoints(Team team);

    Multimap<Team, MapLocation> getSpawnPoints();
}
