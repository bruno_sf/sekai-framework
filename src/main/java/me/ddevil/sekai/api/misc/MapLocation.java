package me.ddevil.sekai.api.misc;

import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.misc.Serializable;

import javax.annotation.Nonnull;
import java.util.Map;

public class MapLocation implements Serializable {
    private static final String X_IDENTIFIER = "x";
    private static final String Y_IDENTIFIER = "y";
    private static final String Z_IDENTIFIER = "z";
    private static final String YAW_IDENTIFIER = "yaw";
    private static final String PITCH_IDENTIFIER = "pitch";
    public static final String MAP_LOCATION_IDENTIFIER = "location";

    private double x;
    private double y;
    private double z;
    private float yaw;
    private float pitch;

    public MapLocation(double x, double y, double z, float yaw, float pitch) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public MapLocation(double x, double y, double z) {
        this(x, y, z, 0, 0);
    }

    public MapLocation(Map<String, Object> map) {
        this.x = ((Number) map.get(X_IDENTIFIER)).doubleValue();
        this.y = ((Number) map.get(Y_IDENTIFIER)).doubleValue();
        this.z = ((Number) map.get(Z_IDENTIFIER)).doubleValue();
        this.yaw = ((Number) map.get(YAW_IDENTIFIER)).floatValue();
        this.pitch = ((Number) map.get(PITCH_IDENTIFIER)).floatValue();
    }

    public MapLocation(MapLocation position) {
        this(position.getX(), position.getY(), position.getZ(), position.getYaw(), position.getPitch());
    }


    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public float getYaw() {
        return yaw;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }


    public int getBlockX() {
        return (int) x;
    }

    public int getBlockY() {
        return (int) y;
    }

    public int getBlockZ() {
        return (int) z;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MapLocation that = (MapLocation) o;

        if (Integer.compare(that.getBlockX(), getBlockX()) != 0) return false;
        if (Integer.compare(that.getBlockY(), getBlockY()) != 0) return false;
        return Integer.compare(that.getBlockZ(), getBlockZ()) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getX());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getY());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getZ());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }


    @Nonnull
    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .put(X_IDENTIFIER, x)
                .put(Y_IDENTIFIER, y)
                .put(Z_IDENTIFIER, z)
                .put(YAW_IDENTIFIER, yaw)
                .put(PITCH_IDENTIFIER, pitch)
                .build();
    }

    public static MapLocation deserialize(Map<String, Object> map) {
        double x = (double) map.get(X_IDENTIFIER);
        double y = (double) map.get(Y_IDENTIFIER);
        double z = (double) map.get(Z_IDENTIFIER);
        double yaw = (double) map.get(YAW_IDENTIFIER);
        double pitch = (double) map.get(PITCH_IDENTIFIER);
        return new MapLocation(x, y, z, (float) yaw, (float) pitch);
    }

    @Override
    public String toString() {
        return "MapLocation{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
