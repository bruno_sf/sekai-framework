package me.ddevil.sekai.api.misc;

import com.google.common.collect.ImmutableMap;
import me.ddevil.sekai.api.model.Model;
import me.ddevil.sekai.api.resource.GameMapResources;
import me.ddevil.sekai.exception.DependenciesNotSupportedException;
import me.ddevil.sekai.internal.model.BaseModel;
import me.ddevil.shiroi.utils.item.MinecraftIcon;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * Created by bruno on 03/10/2016.
 */
public class MapSetting extends BaseModel {
    private static final String WEIGHT_IDENTIFIER = "weight";
    private int weight;
    private MinecraftIcon icon;

    public MapSetting(String name, String alias, int weight, MinecraftIcon icon) {
        super(name, alias);
        this.weight = weight;
        this.icon = icon;
    }

    public MapSetting(Map<String, Object> map) {
        super(map);
        this.weight = ((Number) map.get(WEIGHT_IDENTIFIER)).intValue();
        this.icon = MinecraftIcon.deserialize((Map<String, Object>) map.get(MinecraftIcon.ICON_IDENTIFIER));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MapSetting that = (MapSetting) o;

        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Nonnull
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .putAll(super.serialize())
                .put(WEIGHT_IDENTIFIER, weight)
                .put(MinecraftIcon.ICON_IDENTIFIER, icon.serialize())
                .build();
    }

    @Override
    public String toString() {
        return "MapSetting{" +
                "weight=" + weight +
                ", name=" + name +
                '}';
    }

    @Override
    public boolean isDependency(Model model) {
        return false;
    }

    @Override
    public void addDependency(Model model) {
        throw new DependenciesNotSupportedException(this);
    }

    @Override
    public Set<Model> getAllDependencies() {
        return Collections.emptySet();
    }
}
