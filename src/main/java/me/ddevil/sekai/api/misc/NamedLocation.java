package me.ddevil.sekai.api.misc;

import com.google.common.collect.ImmutableMap;
import me.ddevil.shiroi.misc.Nameable;
import me.ddevil.shiroi.utils.serialization.SerializationConstants;

import javax.annotation.Nonnull;
import javax.annotation.OverridingMethodsMustInvokeSuper;
import java.util.Map;


public class NamedLocation extends MapLocation implements Nameable {

    @Nonnull
    private String name;
    @Nonnull
    private String alias;

    public NamedLocation(@Nonnull String name, @Nonnull String alias, double x, double y, double z, float yaw, float pitch) {
        super(x, y, z, yaw, pitch);
        this.name = name;
        this.alias = alias;
    }

    public NamedLocation(Map<String, Object> map) {
        super(map);
        this.name = String.valueOf(map.get(SerializationConstants.NAMEABLE_NAME_IDENTIFIER));
        this.alias = String.valueOf(map.get(SerializationConstants.NAMEABLE_ALIAS_IDENTIFIER));
    }

    public NamedLocation(String name, String alias, int x, int y, int z) {
        this(name, alias, x, y, z, 0, 0);
    }

    @Nonnull
    @OverridingMethodsMustInvokeSuper
    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .putAll(super.serialize())
                .put(SerializationConstants.NAMEABLE_NAME_IDENTIFIER, name)
                .put(SerializationConstants.NAMEABLE_ALIAS_IDENTIFIER, alias)
                .build();
    }

    @Nonnull
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(@Nonnull String name) {
        this.name = name;
    }

    @Nonnull
    @Override
    public String getAlias() {
        return alias;
    }

    @Override
    public void setAlias(@Nonnull String alias) {
        this.alias = alias;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        NamedLocation that = (NamedLocation) o;

        if (!name.equals(that.name)) return false;
        return alias.equals(that.alias);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + alias.hashCode();
        return result;
    }
}
