package me.ddevil.sekai.api.misc;

import me.ddevil.shiroi.misc.Nameable;
import me.ddevil.shiroi.utils.design.ColorDesign;

/**
 * // TODO: 15/10/2016 documentation
 */
public interface Team extends Nameable {
    ColorDesign getColorDesign();

    void setColorDesign(ColorDesign colorDesign);
}
