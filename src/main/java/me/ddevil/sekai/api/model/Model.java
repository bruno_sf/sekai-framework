package me.ddevil.sekai.api.model;

import me.ddevil.sekai.api.resource.Resource;
import me.ddevil.shiroi.misc.Nameable;

import java.util.Set;

/**
 * Represents a "template" that a {@link me.ddevil.sekai.api.game.GameObject} can
 * use to load default parameters.
 * <br>
 * A model can contain other models as dependencies. If present, these "dependencies" should be loaded on the deserialization constructor.
 */
public interface Model extends Resource, Nameable {

    boolean hasDependencies();

    boolean isDependency(Model model);

    void addDependency(Model model);

    Set<Model> getAllDependencies();

}
