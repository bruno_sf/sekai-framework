package me.ddevil.sekai.api.resource;

import me.ddevil.sekai.api.game.GameObject;
import me.ddevil.sekai.api.map.GameMap;
import me.ddevil.sekai.api.model.Model;
import me.ddevil.sekai.api.misc.MapSetting;
import me.ddevil.shiroi.misc.Nameable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * A.K.A. Resource Path
 * <br>
 * Represents literally a resource path with every single resource ({@link Model}) used by a {@link me.ddevil.sekai.api.map.GameMap}.
 * This is commonly loaded by a deserialization constructor, since it doesn't implements {@link Model} and cannot be loaded by a {@link ModelProvider}.
 * <br>
 * When serialized, the include models are not serialized inside the file itself. They are
 * only saved as a reference, which means there will be a list with the name of every included model.
 * <br>
 * When deserialized, these models should be loaded through a {@link ModelProvider}, and it should happen
 * during the construction, via the deserialization constructor, which is usually defined as something like:
 * <br>
 * <code> new GameMapResources(Map&lt String, Object&gt map, ModelProvider handler)</code>
 */
public interface GameMapResources extends Resource, Iterable<Model> {
    /**
     * Gets every loaded model with the given <code>modelClass</code> and
     *
     * @param modelClass The model type to be used.
     * @param <M>        The model type to be used, determined by <code>modelClass</code>.
     * @return A list with every model that matches the <code>modelClass</code> type.
     * <br>
     * If there are no matches, the list will be empty.
     */
    @Nonnull
    <M extends Model> List<M> getLoadedModels(Class<M> modelClass);

    /**
     * Searches for a model in the resource path that matches the given <code>modelName</code> and <code>modelClass</code>.
     * <br>
     * If there are no matches, returns null.
     *
     * @param modelName  The desired model's name.
     * @param modelClass The desired model's type.
     * @param <M>        The desired model's type, determined by <code>modelClass</code>.
     * @return The model found with the given name and type, or null if none was found.
     */
    @Nullable
    <M extends Model> M getModel(String modelName, Class<M> modelClass);

    /**
     * @param modelName
     * @return The model loaded  with the specified <code>modelName</code>
     * <br>
     * <code>null</code> if there isn't any model with the specified <code>modelName</code>
     */
    @Nullable
    Model getModel(String modelName);

    /**
     * Attempts to get a {@link Model} through {@link #getModel(String)}, if it returns null, uses the given <code>handler</code> to
     * attempt to loadMap a new one, if that fails, returns null.
     *
     * @param modelName The desired model's name
     * @param handler   The handler to use if {@link #getModel(String)} fails.
     * @return The found {@link Model}, or <code>null</code> if none was found.
     * @throws Exception {@link ModelProvider#provide(String)} may return an exception.
     */
    @Nullable
    default Model getModel(String modelName, ModelProvider<?> handler) throws Exception {
        Model model = getModel(modelName);
        if (model == null) {
            model = handler.provide(modelName);
        }
        return model;
    }

    /**
     * Get's every model loaded by the resource path. Like {@link #getAllModelsMap()}, but as a Collection.
     * <br>
     * Will be empty if there isn't any model.
     *
     * @return A collection with every model loaded by the resource path.
     */
    @Nonnull
    Collection<Model> getAllModels();

    /**
     * @return A {@link Map}&lt NameOfTheModel, Model&gt containing all of the models.
     * <br>
     * Looks like {@link #getAllModels()}, but as a map.
     */
    @Nonnull
    Map<String, Model> getAllModelsMap();

    boolean hasResource(@Nonnull String resourceName);

    /**
     * Adds the given <code>resource</code> to the resource path.
     * Any class that implements {@link Model} can be accepted, like {@link MapSetting} for example.
     * <br>
     *
     * @param resource The resource to be added.
     * @throws IllegalArgumentException If there is already a Model with the same name loaded.
     */
    void addResource(@Nonnull Model resource) throws IllegalArgumentException;

    /**
     * Check for if the resource path contains all of the models needed by the <code>model</code>
     * parameter to work, if it doesn't, it will attempt to loadMap
     *
     * @param model The model to be checked
     * @return <code>true</code> if all of the required dependencies are available or were loaded using the given <code>handler</code><br>
     * <code>false</code> if there are still missing dependencies, usually happens when the <code>handler</code> wasn't able to provide all the required dependencies
     */
    boolean checkDependencies(@Nonnull Model model, @Nonnull ModelProvider<?> handler) throws Exception;

    /**
     * @return A list containing all of the loaded MapSettings by this resource path
     */
    @Nonnull
    List<MapSetting> getMapSettings();

    /**
     * Removes every model declared that isn't a dependency of the given game objects
     *
     * @param objects The objects that should be used as reference for the cleaning.
     *                <br>
     *                I recommend you to use {@link GameMap#getGameObjects()} as a parameters for this.
     */
    void clean(List<GameObject<?>> objects);

    /**
     * @param setting The name of the desired {@link MapSetting}
     * @return The {@link MapSetting} that is loaded with the given <code>setting</code> name or null if there is no MapSetting with that name.
     * @see Nameable#getName()
     */
    @Nullable
    MapSetting getMapSetting(String setting);

    boolean hasResource(Model s);
}
