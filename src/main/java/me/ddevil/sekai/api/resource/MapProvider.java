package me.ddevil.sekai.api.resource;

import me.ddevil.sekai.api.map.GameMap;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @param <M> The type of the {@link GameMap} it is responsible for || can handle
 * @param <R> The type of the {@link GameMapResources} it is responsible for || can handle
 */
public interface MapProvider<M extends GameMap<R>, R extends GameMapResources> {
    void save(M map, ModelProvider<?> provider) throws IOException;

    M loadMap(String mapName) throws IOException, ParseException;

    R loadResources(Map<String, Object> o);
}
