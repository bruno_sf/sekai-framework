package me.ddevil.sekai.api.resource;


import me.ddevil.sekai.api.model.Model;
import me.ddevil.shiroi.misc.Nameable;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * A ModelProvider is responsible for loading, saving, and providing certain models.
 *
 * @param <R> The type of the {@link GameMapResources} it requires to provide the models it is capable of providing.
 */
public interface ModelProvider<R extends GameMapResources> {

    /**
     * Provides
     * <br>
     * Commonly used during deserialization to resolve dependencies.
     *
     * @param modelName     The name of the model that you wish to get.
     * @param expectedModel The type of the model that you wish to get.
     * @param <M>           Type parameter of the model that you wish to get. Should be defined by <code>expectedModel</code>.
     * @return A model that matches the given <code>modelName</code> and <code>expectedModel</code>
     * @see Nameable#getName()
     */
    <M extends Model> M provide(String modelName, Class<M> expectedModel, Optional<R> resources) throws IOException, ParseException;

    default <M extends Model> M provide(String modelName, Class<M> expectedModel) throws IOException, ParseException {
        return provide(modelName, expectedModel, null);
    }

    Model provide(String modelName) throws IOException, ParseException, ClassNotFoundException;

    void save(Model model) throws IOException;

    default boolean canHandle(Class<? extends Model> model) {
        return getHandledTypes().contains(model);
    }

    List<Class<? extends Model>> getHandledTypes();
}
