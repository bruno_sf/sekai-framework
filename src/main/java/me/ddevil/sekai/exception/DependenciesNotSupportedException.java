package me.ddevil.sekai.exception;

import me.ddevil.sekai.api.model.Model;

/**
 * Created by bruno on 15/10/2016.
 */
public class DependenciesNotSupportedException extends IllegalStateException {
    private final Model model;

    public DependenciesNotSupportedException(Model model) {
        super(model + " doesn't support dependencies!");
        this.model = model;
    }

    public Model getModel() {
        return model;
    }

}
