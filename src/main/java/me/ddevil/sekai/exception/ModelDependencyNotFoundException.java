package me.ddevil.sekai.exception;

import me.ddevil.sekai.api.model.Model;

import javax.annotation.Nonnull;

/**
 * Created by bruno on 13/10/2016.
 */
public class ModelDependencyNotFoundException extends IllegalArgumentException {
    @Nonnull
    private final String model;

    public ModelDependencyNotFoundException(@Nonnull String model) {
        this.model = model;
    }

    public ModelDependencyNotFoundException(String s, @Nonnull String model) {
        super(s);
        this.model = model;
    }

    public ModelDependencyNotFoundException(String message, Throwable cause, @Nonnull String model) {
        super(message, cause);
        this.model = model;
    }

    public ModelDependencyNotFoundException(Throwable cause, @Nonnull String model) {
        super(cause);
        this.model = model;
    }

    public ModelDependencyNotFoundException(@Nonnull String model, @Nonnull Class<? extends Model> modelClass) {
        super("Couldn't find required " + modelClass.getSimpleName() + " " + model);
        this.model = model;
    }

    public ModelDependencyNotFoundException(Throwable cause, @Nonnull String model, @Nonnull Class<? extends Model> modelClass) {
        super("Couldn't find required " + modelClass.getSimpleName() + " " + model, cause);
        this.model = model;
    }

    @Nonnull
    public String getModel() {
        return model;
    }
}
