package me.ddevil.sekai.exception;

import me.ddevil.sekai.api.model.Model;

import java.io.IOException;

public class ModelSaveException extends IllegalStateException {
    public ModelSaveException(Model model) {
        super("Couldn't save model " + model.getFullName() + "...");
    }
}
