package me.ddevil.sekai.internal.game;

import com.google.common.collect.ImmutableMap;
import me.ddevil.sekai.api.game.GameObject;
import me.ddevil.sekai.api.misc.MapLocation;
import me.ddevil.sekai.api.model.Model;
import me.ddevil.sekai.api.resource.ModelProvider;
import me.ddevil.shiroi.misc.internal.BaseNameable;
import org.json.simple.parser.ParseException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.OverridingMethodsMustInvokeSuper;
import java.io.IOException;
import java.util.Map;

/**
 * Created by bruno on 13/10/2016.
 */
public abstract class BaseGameObject<M extends Model> extends BaseNameable implements GameObject<M> {
    private static final String MODEL_IDENTTIFIER = "model";
    @Nonnull
    protected MapLocation position;
    @Nullable
    protected M model;

    public BaseGameObject(@Nonnull String name, @Nonnull String alias, @Nonnull MapLocation position) {
        super(name, alias);
        this.position = position;
    }

    public BaseGameObject(@Nonnull Map<String, Object> map, ModelProvider<?> provider) throws ParseException, IOException, ClassNotFoundException {
        super(map);
        this.position = MapLocation.deserialize((Map<String, Object>) map.get(MapLocation.MAP_LOCATION_IDENTIFIER));
        if (map.containsKey(MODEL_IDENTTIFIER)) {
            this.model = (M) provider.provide(String.valueOf(map.get(MODEL_IDENTTIFIER)));
        }
    }


    public BaseGameObject(String name, String alias, MapLocation position, M model) {
        super(name, alias);
        this.position = position;
        this.model = model;
    }

    @Nonnull
    @OverridingMethodsMustInvokeSuper
    @Override
    public Map<String, Object> serialize() {
        ImmutableMap.Builder<String, Object> builder = new ImmutableMap.Builder<String, Object>()
                .putAll(super.serialize())
                .put(MapLocation.MAP_LOCATION_IDENTIFIER, position.serialize());
        if (model != null) {
            builder.put(MODEL_IDENTTIFIER, model.getName());
        }
        return builder
                .build();
    }

    @Nonnull
    @Override
    public MapLocation getPosition() {
        return new MapLocation(position);
    }

    @Override
    public void setPosition(@Nonnull MapLocation position) {
        this.position = position;
    }

    @Nullable
    @Override
    public M getModel() {
        return model;
    }

    @Override
    public void setModel(M model) {
        this.model = model;
    }
}
