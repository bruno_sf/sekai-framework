package me.ddevil.sekai.internal.map;

import com.google.common.collect.ImmutableMap;
import me.ddevil.sekai.SekaiConstants;
import me.ddevil.sekai.api.map.FFAGameMap;
import me.ddevil.sekai.api.resource.GameMapResources;
import me.ddevil.sekai.api.misc.MapLocation;
import me.ddevil.sekai.api.resource.MapProvider;
import me.ddevil.sekai.api.resource.ModelProvider;
import me.ddevil.shiroi.utils.item.MinecraftIcon;
import me.ddevil.shiroi.utils.serialization.SerializationUtils;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by bruno on 24/09/2016.
 */
public abstract class BaseFFAGameMap<R extends GameMapResources> extends BaseGameMap<R> implements FFAGameMap<R> {
    @Nonnull
    private final List<MapLocation> spawnPoints;
    private final int maxPlayers;

    public BaseFFAGameMap(String name, String alias, R resources, @Nonnull MinecraftIcon icon, @Nonnull List<String> description, @Nonnull List<MapLocation> spawnPoints, int maxPlayers) {
        super(name, alias, resources, icon, description);
        this.spawnPoints = spawnPoints;
        this.maxPlayers = maxPlayers;
    }

    public BaseFFAGameMap(Map<String, Object> map, MapProvider<?, R> resources) {
        super(map, resources);
        this.maxPlayers = ((Number) map.get(SekaiConstants.MAP_MAX_PLAYERS_IDENTIFIER)).intValue();
        List<Map<String, Object>> serializedMapSpawnPoints = (List<Map<String, Object>>) map.get(SekaiConstants.SPAWN_POINTS_IDENTIFIER);
        this.spawnPoints = SerializationUtils.deserializeList(serializedMapSpawnPoints, MapLocation::deserialize);
    }

    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .putAll(super.serialize())
                .put(SekaiConstants.SPAWN_POINTS_IDENTIFIER, SerializationUtils.serializeList(spawnPoints))
                .build();
    }

    @Override
    public List<MapLocation> getSpawnPoints() {
        return new ArrayList<>(spawnPoints);
    }

    @Override
    public int getMaxPlayers() {
        return maxPlayers;
    }
}
