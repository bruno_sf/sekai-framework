package me.ddevil.sekai.internal.map;

import com.google.common.collect.ImmutableMap;
import me.ddevil.sekai.SekaiConstants;
import me.ddevil.sekai.api.model.Model;
import me.ddevil.sekai.api.game.GameObject;
import me.ddevil.sekai.api.map.GameMap;
import me.ddevil.sekai.api.resource.GameMapResources;
import me.ddevil.sekai.api.resource.MapProvider;
import me.ddevil.sekai.api.resource.ModelProvider;
import me.ddevil.shiroi.misc.internal.BaseNameable;
import me.ddevil.shiroi.utils.item.MinecraftIcon;

import javax.annotation.Nonnull;
import javax.annotation.OverridingMethodsMustInvokeSuper;
import java.util.List;
import java.util.Map;

public abstract class BaseGameMap<R extends GameMapResources> extends BaseNameable implements GameMap<R> {
    private static final String MAP_ICON_IDENTIFIER = "icon";
    private static final String MAP_DESCRIPTION_IDENTIFIER = "description";
    protected R resources;
    @Nonnull
    private MinecraftIcon icon;
    @Nonnull
    private List<String> description;

    public BaseGameMap(String name, String alias, R resources, @Nonnull MinecraftIcon icon, @Nonnull List<String> description) {
        super(name, alias);
        this.resources = resources;
        this.icon = icon;
        this.description = description;
    }

    public BaseGameMap(Map<String, Object> map, MapProvider<?, R> resources) {
        super(map);
        this.resources = resources.loadResources((Map<String, Object>) map.get(SekaiConstants.MAP_RESOURCES_IDENTIFIER));
        this.icon = MinecraftIcon.deserialize((Map<String, Object>) map.get(MAP_ICON_IDENTIFIER));
        this.description = (List<String>) map.get(MAP_DESCRIPTION_IDENTIFIER);
    }

    @Override
    public R getResources() {
        return resources;
    }

    @Override
    public void setResources(R resources) {
        this.resources = resources;
    }

    @Override
    public void addObject(@Nonnull GameObject<?> object) {
        Model model = object.getModel();
        if (model != null) {
            if (!resources.hasResource(model)) {
                System.out.println("Adding resource "+model);
                resources.addResource(model);
            }
            if (model.hasDependencies()) {
                //Check for any dependency that are not loaded in the resource path
                model.getAllDependencies().stream().filter(s -> !resources.hasResource(s)).forEach(s -> {
                    resources.addResource(model);
                });
            }
        }
        addObject0(object);
    }

    protected abstract void addObject0(GameObject<?> object);

    @Nonnull
    @Override
    public MinecraftIcon getIcon() {
        return icon;
    }

    @Override
    public void setIcon(@Nonnull MinecraftIcon icon) {
        this.icon = icon;
    }

    @Override
    @Nonnull
    public List<String> getDescription() {
        return description;
    }

    @Override
    public void setDescription(@Nonnull List<String> description) {
        this.description = description;
    }

    @Override
    @OverridingMethodsMustInvokeSuper
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .putAll(super.serialize())
                .put(MAP_ICON_IDENTIFIER, icon.serialize())
                .put(MAP_DESCRIPTION_IDENTIFIER, description)
                .put(SekaiConstants.MAP_MAX_PLAYERS_IDENTIFIER, getMaxPlayers())
                .put(SekaiConstants.MAP_RESOURCES_IDENTIFIER, resources.serialize())
                .build();
    }
}
