package me.ddevil.sekai.internal.map;

import com.google.common.collect.ImmutableMap;
import me.ddevil.sekai.SekaiConstants;
import me.ddevil.sekai.api.resource.GameMapResources;
import me.ddevil.sekai.api.misc.MapSetting;
import me.ddevil.sekai.api.model.Model;
import me.ddevil.sekai.api.resource.MapProvider;
import me.ddevil.sekai.api.resource.ModelProvider;
import me.ddevil.sekai.exception.ModelDependencyNotFoundException;
import me.ddevil.shiroi.utils.serialization.SerializationUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.OverridingMethodsMustInvokeSuper;
import java.util.*;
import java.util.logging.Level;

/**
 * Created by bruno on 12/10/2016.
 */
public abstract class BaseGameMapResources implements GameMapResources {
    private static final String MAP_SETTINGS_IDENTIFIER = "mapSettings";
    protected final List<MapSetting> mapSettings;

    public BaseGameMapResources() {
        mapSettings = new ArrayList<>();
    }

    public BaseGameMapResources(Map<String, Object> map, ModelProvider<?> handler) {
        List<String> requiredMapSettings = (List<String>) map.get(MAP_SETTINGS_IDENTIFIER);
        this.mapSettings = SerializationUtils.deserializeListFromReference(requiredMapSettings, s -> {
            try {
                return handler.provide(s, MapSetting.class);
            } catch (Exception e) {
                throw new ModelDependencyNotFoundException(s, MapSetting.class);
            }
        });
    }


    @Override
    public boolean checkDependencies(@Nonnull Model model, @Nonnull ModelProvider<?> handler) {
        if (!model.hasDependencies()) {
            return true;
        }
        for (Model dependency : model.getAllDependencies()) {
            try {
                addResource(dependency);
            } catch (Exception e) {
                SekaiConstants.LOGGER.log(Level.SEVERE, "There was an error while trying to get dependency " + dependency + " of model " + model.getName() + "(" + model.getClass() + ")", e);
                return false;
            }
        }
        return false;
    }


    @Override
    public boolean hasResource(Model s) {
        return hasResource(s.getName());
    }

    @Nonnull
    @Override
    public Map<String, Model> getAllModelsMap() {
        ImmutableMap.Builder<String, Model> builder = new ImmutableMap.Builder<>();
        builder.putAll(getAllModelsMap0());
        mapSettings.forEach(mapSetting -> builder.put(mapSetting.getName(), mapSetting));
        return builder.build();
    }

    protected abstract Map<String, Model> getAllModelsMap0();

    @Override
    public boolean hasResource(@Nonnull String resourceName) {
        return getAllModelsMap().containsKey(resourceName);
    }

    @Override
    public void addResource(@Nonnull Model resource) throws IllegalArgumentException {
        if (hasResource(resource.getName())) {
            throw new IllegalArgumentException("There is already a model similar to " + resource);
        }
        if (resource instanceof MapSetting) {
            mapSettings.add((MapSetting) resource);
            return;
        }
        addResource0(resource);
    }

    protected abstract void addResource0(Model resource);

    @Override
    public Iterator<Model> iterator() {
        return getAllModels().iterator();
    }

    @Nonnull
    @OverridingMethodsMustInvokeSuper
    @Override
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .put(MAP_SETTINGS_IDENTIFIER, SerializationUtils.serializeListToReference(mapSettings))
                .build();
    }

    @Nonnull
    @Override
    public Collection<Model> getAllModels() {
        return getAllModelsMap().values();
    }

    @Nonnull
    @Override
    public List<MapSetting> getMapSettings() {
        return new ArrayList<>(mapSettings);
    }

    @Nullable
    @Override
    public MapSetting getMapSetting(String setting) {
        for (MapSetting mapSetting : mapSettings) {
            if (mapSetting.getName().equals(setting)) {
                return mapSetting;
            }
        }
        return null;
    }
}
