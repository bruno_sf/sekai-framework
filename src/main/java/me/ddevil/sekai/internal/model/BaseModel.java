package me.ddevil.sekai.internal.model;

import com.google.common.collect.ImmutableMap;
import me.ddevil.sekai.api.model.Model;
import me.ddevil.sekai.api.resource.GameMapResources;
import me.ddevil.shiroi.misc.internal.BaseNameable;

import javax.annotation.Nonnull;
import javax.annotation.OverridingMethodsMustInvokeSuper;
import java.util.Map;

public abstract class BaseModel extends BaseNameable implements Model {

    public static final String MODEL_TYPE_IDENTIFIER = "type";

    public BaseModel(String name, String alias) {
        super(name, alias);
    }

    public BaseModel(Map<String, Object> map) {
        super(map);
    }


    @Override
    public boolean hasDependencies() {
        return !getAllDependencies().isEmpty();
    }

    @Override
    @Nonnull
    @OverridingMethodsMustInvokeSuper
    public Map<String, Object> serialize() {
        return new ImmutableMap.Builder<String, Object>()
                .putAll(super.serialize())
                .put(MODEL_TYPE_IDENTIFIER, getClass().getName())
                .build();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + name + "/" + alias + ")";
    }
}
