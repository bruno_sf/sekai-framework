package me.ddevil.sekai.internal.model;

import me.ddevil.sekai.api.model.Model;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * Created by bruno on 17/10/2016.
 */
public class IndependentModel extends BaseModel {
    public IndependentModel(String name, String alias) {
        super(name, alias);
    }

    public IndependentModel(Map<String, Object> map) {
        super(map);
    }


    @Override
    public boolean isDependency(Model model) {
        return false;
    }

    @Override
    public void addDependency(Model model) {
        throw new UnsupportedOperationException(getClass().getName() + " doesn't take any dependencies!");
    }

    @Override
    public Set<Model> getAllDependencies() {
        return Collections.emptySet();
    }
}
