package me.ddevil.sekai.internal.resource;

import me.ddevil.sekai.SekaiConstants;
import me.ddevil.sekai.api.map.GameMap;
import me.ddevil.sekai.api.model.Model;
import me.ddevil.sekai.api.resource.GameMapResources;
import me.ddevil.sekai.api.resource.MapProvider;
import me.ddevil.sekai.api.resource.ModelProvider;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * Created by bruno on 17/10/2016.
 */
public abstract class LocalMapProvider<M extends GameMap<R>, R extends GameMapResources> implements MapProvider<M, R> {
    @Nonnull
    private File resourcesFolder;

    public LocalMapProvider(@Nonnull File resourcesFolder) {
        this.resourcesFolder = resourcesFolder;
    }

    @Nonnull
    public File getResourcesFolder() {
        return resourcesFolder;
    }

    public void setResourcesFolder(@Nonnull File resourcesFolder) {
        this.resourcesFolder = resourcesFolder;
    }

    @Override
    public M loadMap(String mapName) throws IOException, ParseException {
        File file = getFile(mapName);
        return loadMap0((JSONObject) new JSONParser().parse(new FileReader(file)));
    }

    protected abstract M loadMap0(JSONObject json);

    @Override
    public void save(M map, ModelProvider<?> provider) throws IOException {
        R resources = map.getResources();
        Collection<Model> models = resources.getAllModels();
        SekaiConstants.LOGGER.info("Saving " + map.getFullName() + "'s dependencies(" + models.size() + ")...");
        int i = 1;
        for (Model object : models) {
            try {
                SekaiConstants.LOGGER.info("Saving dependency " + i + '/' + models.size());
                provider.save(object);
            } catch (Exception e) {
                throw new IllegalStateException("There was an error while trying to save map " + map.getFullName(), e);
            }
            i++;
        }
        File file = getFile(map);
        SekaiConstants.LOGGER.info("Saving map " + map.getFullName() + " to '" + file.getPath() + "'");

        FileWriter writer = new FileWriter(file);
        writer.write(new JSONObject(map.serialize()).toJSONString());
        writer.close();
    }

    private File getFile(M map) {
        return getFile(map.getName());
    }

    private JSONObject getJson(String modelName) throws IOException, ParseException {
        return getJson(getFile(modelName));
    }

    private JSONObject getJson(File file) throws IOException, ParseException {
        return (JSONObject) new JSONParser().parse(new FileReader(file));
    }

    private File getFile(String mapName) {
        if (resourcesFolder.isFile()) {
            throw new IllegalStateException("Resource folder is a file and not a folder!");
        }
        return new File(resourcesFolder, mapName + SekaiConstants.MODEL_EXTENSION);
    }
}
