package me.ddevil.sekai.internal.resource;

import me.ddevil.sekai.SekaiConstants;
import me.ddevil.sekai.api.resource.GameMapResources;
import me.ddevil.sekai.api.misc.MapSetting;
import me.ddevil.sekai.api.model.Model;
import me.ddevil.sekai.api.resource.ModelProvider;
import me.ddevil.sekai.exception.ModelSaveException;
import me.ddevil.sekai.internal.model.BaseModel;
import me.ddevil.shiroi.misc.Serializable;
import me.ddevil.shiroi.utils.serialization.SerializationConstants;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.annotation.Nullable;
import java.io.*;
import java.util.Optional;

/**
 * Created by bruno on 12/10/2016.
 */
public abstract class LocalModelProvider<R extends GameMapResources> implements ModelProvider<R> {
    @Nullable
    private File resourcesFolder;


    public LocalModelProvider(File resourcesFolder) {
        this.resourcesFolder = resourcesFolder;
        if (resourcesFolder != null) {
            if (resourcesFolder.isFile()) {
                throw new IllegalArgumentException(resourcesFolder.getPath() + " must be a folder and not a file!");
            }
        }
    }

    @Nullable
    public File getResourcesFolder() {
        return resourcesFolder;
    }

    public void setResourcesFolder(@Nullable File resourcesFolder) {
        this.resourcesFolder = resourcesFolder;
    }

    @Override
    public <M extends Model> M provide(String modelName, Class<M> expectedModel) throws IOException, ParseException {
        return provide(modelName, expectedModel, Optional.empty());
    }

    private JSONObject loadJSON(String modelName) throws IOException, ParseException {
        return loadJSON(getFile(modelName));
    }

    @Override
    public <M extends Model> M provide(String modelName, Class<M> expectedModel, Optional<R> resources) throws IOException, ParseException {
        JSONObject json = loadJSON(modelName);
        if (expectedModel.equals(MapSetting.class)) {
            return (M) new MapSetting(json);
        }
        return provide(json, expectedModel, Optional.empty());
    }

    public abstract <M extends Model> M provide(JSONObject json, Class<M> modelClass, Optional<R> resources);


    @Override
    public Model provide(String modelName) throws IOException, ParseException, ClassNotFoundException {
        JSONObject json = getJson(modelName);
        String modelType = String.valueOf(json.get(BaseModel.MODEL_TYPE_IDENTIFIER));
        Class<? extends Model> aClass = (Class<? extends Model>) Class.forName(modelType);
        return provide(modelName, aClass);
    }

    private JSONObject getJson(String modelName) throws IOException, ParseException {
        return getJson(getFile(modelName));
    }

    private JSONObject getJson(File file) throws IOException, ParseException {
        return (JSONObject) new JSONParser().parse(new FileReader(file));
    }


    protected File getFile(String modelName) throws IllegalStateException {
        if (resourcesFolder == null) {
            throw new IllegalStateException("There is no resource folder defined!");
        }
        if (resourcesFolder.isFile()) {
            throw new IllegalStateException("Resource folder is a file and not a folder!");
        }
        return new File(resourcesFolder, modelName + SekaiConstants.MODEL_EXTENSION);
    }

    protected JSONObject loadJSON(File file) throws IOException, ParseException {
        return (JSONObject) new JSONParser().parse(new BufferedReader(new FileReader(file)));
    }

    @Override
    public void save(Model model) throws IOException {
        File file = getFile(model);
        SekaiConstants.LOGGER.info("Saving model " + model.getFullName() + " to '" + file.getPath() + "'");
        model.getAllDependencies().forEach((model1) -> {
            try {
                save(model1);
            } catch (Exception e) {
                throw new ModelSaveException(model1);
            }
        });
        save(file, model);
    }

    private File getFile(Model model) {
        return getFile(model.getName());
    }

    private void save(File file, Serializable serializable) throws IOException {
        FileWriter writer = new FileWriter(file);
        writer.write(new JSONObject(serializable.serialize()).toJSONString());
        writer.close();
    }


}
